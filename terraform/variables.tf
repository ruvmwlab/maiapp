variable "vsphere_server" {
  default = "quinzy.kernarc.local"
}

variable "vsphere_user" {
  default = "devops@vsphere.local"
}

variable "vsphere_pass" {}
variable "root_pass" {}

variable "vsphere_datacenter" {
  default = "KERNARC"
}

variable "vsphere_network" {
  default = "INNER"
}

variable "vsphere_template" {
  default = "MAIDevOps_unit"
}

variable "vsphere_cluster" {
  default = "Main"
}

variable "vsphere_datastore" {
  default = "SSD512"
}
