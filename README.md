# Welcome to MAI DevOps Course

Этапы курса:

* Освоить репозиторий кода [+]

* Развертывание виртуальной машины (ВМ) с помощью Terraform

* Конфигурирование ВМ с помощью Ansible
